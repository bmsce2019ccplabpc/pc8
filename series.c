#include<stdio.h>
#include<math.h>
void sum(void)
{
    int i, n, fac=1;
    float sum=0.0;
    printf("Enter number of terms");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
      fac=fac*i;
      sum=sum+(float)((pow(i,i))/fac);
    }
    printf("Sum of series is %f ",sum);
}
int main()
{
    sum();
    return 0;    
}
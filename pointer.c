#include<stdio.h>
void swap(int *a, int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1, n2;
printf("ENTER FIRST NUMBER ");
scanf("%d",&n1);
printf("ENTER SECOND NUMBER ");
scanf("%d",&n2);
printf("VALUE OF n1 AND n2 BEFORE FUNCTION CALL IS %d, %d ",n1,n2);
printf("\n");
swap(&n1,&n2);
printf("VALUE OF n1 AND n2 AFTER FUNCTION CALL IS %d, %d ",n1,n2);
return 0;
}